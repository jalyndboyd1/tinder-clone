import React from "react";
import "./SwipeButtons.css";
import ReplayIcon from "@material-ui/icons/Replay";
import CloseIcon from "@material-ui/icons/Close";
import StarIcon from "@material-ui/icons/Star";
import FavoriteIcon from "@material-ui/icons/Favorite";
import FlashOnIcon from "@material-ui/icons/FlashOn";
import IconButton from "@material-ui/core/IconButton";

function SwipeButtons() {
  return (
    <div className="swipe-buttons">
      <IconButton className="swipe-repeat">
        <ReplayIcon fontSize="default" />
      </IconButton>
      <IconButton className="swipe-close">
        <CloseIcon fontSize="default" />
      </IconButton>
      <IconButton className="swipe-star">
        <StarIcon fontSize="default" />
      </IconButton>
      <IconButton className="swipe-fav">
        <FavoriteIcon fontSize="default" />
      </IconButton>
      <IconButton className="swipe-flash">
        <FlashOnIcon fontSize="default" />
      </IconButton>
    </div>
  );
}

export default SwipeButtons;
