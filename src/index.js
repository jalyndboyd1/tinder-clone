import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
// import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Header from "./Header";
import TinderCards from "./TinderCards";
import SwipeButtons from "./SwipeButtons";
import Chats from "./Chats";
import ChatScreen from "./ChatScreen";
import Profile from "./Profile";

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route exact path="/">
        <Header />
        <TinderCards />
        <SwipeButtons />
      </Route>
      <Route exact path="/chats">
        <Header backButton="/" />
        <Chats />
      </Route>
      <Route exact path="/chats/:person">
        <Header backButton="/chats" />
        <ChatScreen />
      </Route>
      <Route exact path="/profile">
        <Header />
        <Profile name="Jalyn" employment="Software Engineer" age="22" />
      </Route>
    </Switch>
  </BrowserRouter>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
