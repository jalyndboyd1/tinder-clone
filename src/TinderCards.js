import React, { useState, useEffect } from "react";
import TinderCard from "react-tinder-card";
import "./TinderCards.css";
import db from "./firebase";

function TinderCards() {
  const [people, setPeople] = useState([
    //Tinder Cards hardcoded due to quota exceeding in firebase. Here for DEV purposes.
    {
      name: "Jessica",
      age: 40,
      url:
        "https://pyxis.nymag.com/v1/imgs/ecc/2d5/aa2951d6f9d04e15761cbf4270add97f45-19-newgirl.2x.rhorizontal.w710.jpg",
    },
    {
      name: "Claudia",
      age: 32,
      url: "https://iv1.lisimg.com/image/21361081/637full-claudia-doumit.jpg",
    },
  ]);

  useEffect(() => {
    //By leaving dependencies blank.
    // It will run once when component
    //  loads and now when the variable of people changes.
    const unsubscribe = db.collection("people").onSnapshot((snapshot) =>
      setPeople(
        snapshot.docs.map((doc) => {
          return doc.data();
        })
      )
    );
    return () => {
      unsubscribe();
      //Unsubscribing listener so application does not crash.
    };
    //Everytime the DB changes take a snapshot
    //then place it inside of the people state.
  }, [people]);

  return (
    <div>
      <div className="card-container">
        {people.map((person) => {
          return (
            <TinderCard className="swipe" key={person.name}>
              <div
                className="card"
                style={{
                  backgroundImage: `url(${person.url})`,
                }}

                // preventSwipe={["up", "down"]}
              >
                <h3>
                  {" "}
                  {person.name}, {person.age}
                </h3>
              </div>
            </TinderCard>
          );
        })}
      </div>
    </div>
  );
}

export default TinderCards;
