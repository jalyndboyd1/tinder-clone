import React from "react";
import "./Chats.css";
//Chats component will be the box that holds indiviudal chats.
import Chat from "./Chat";

function Chats() {
  return (
    <div className="chats">
      <Chat
        name="Claudia"
        messages="Hey there!"
        timestamp="20 mins ago"
        profilePic="https://m.media-amazon.com/images/M/MV5BMzJkNjNlNWQtZmJjOS00ZjEzLTlmN2UtMTMzZjA0OTY2YzQ0XkEyXkFqcGdeQXVyMjQyNTYzMDY@._V1_.jpg"
      />
    </div>
  );
}

export default Chats;
