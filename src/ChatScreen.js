import { Avatar } from "@material-ui/core";
import React, { useState } from "react";
import "./ChatScreen.css";

function ChatScreen() {
  const [messages, setMessages] = useState([
    {
      name: "Claudia",
      image:
        "https://m.media-amazon.com/images/M/MV5BMzJkNjNlNWQtZmJjOS00ZjEzLTlmN2UtMTMzZjA0OTY2YzQ0XkEyXkFqcGdeQXVyMjQyNTYzMDY@._V1_.jpg",
      message: "Hey Cutie!",
    },
    {
      name: "Claudia",
      image:
        "https://m.media-amazon.com/images/M/MV5BMzJkNjNlNWQtZmJjOS00ZjEzLTlmN2UtMTMzZjA0OTY2YzQ0XkEyXkFqcGdeQXVyMjQyNTYzMDY@._V1_.jpg",
      message: "Happy that I matched with you",
    },
  ]);
  const [input, setInput] = useState("");

  const handleSend = (e) => {
    e.preventDefault();
    setMessages([...messages, { message: input }]);
    setInput("");
  };

  return (
    <div>
      <p className="chat-screen-timestamp">
        You matched with Claudia on 1/9/21
      </p>
      {messages.map((message) => {
        return message.name ? (
          <div key={Math.random()} className="chat-screen-message">
            <Avatar
              className="chat-screen-avatar"
              alt={message.name}
              src={message.image}
            />
            <p className="chat-screen-text">{message.message}</p>
          </div>
        ) : (
          <div key={Math.random()} className="chat-screen-message">
            <p className="chat-screen-textUser">{message.message}</p>
          </div>
        );
      })}

      <form className="chat-screen-input">
        <input
          value={input}
          type="text"
          placeholder="Send a message.."
          className="input-field"
          onChange={(e) => setInput(e.target.value)}
        />
        <button type="submit" onClick={handleSend} className="input-button">
          Send
        </button>
      </form>
    </div>
  );
}

export default ChatScreen;
