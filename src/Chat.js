import React from "react";
import Avatar from "@material-ui/core/Avatar";
import { Link } from "react-router-dom";
import "./Chat.css";

function Chat({ name, messages, timestamp, profilePic }) {
  return (
    <Link to={`/chats/${name}`}>
      <div className="chat">
        <Avatar className="chat-image" alt={name} src={profilePic} />
        <div className="details">
          <h3>{name}</h3>
          <p>{messages}</p>
        </div>
        <div className="timestamp">
          <p>{timestamp}</p>
        </div>
      </div>
    </Link>
  );
}

export default Chat;
