import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyBDOQSFFQgbeiqFg5NQTmhRWOdJVoHEQjk",
  authDomain: "tinder-clone-93207.firebaseapp.com",
  projectId: "tinder-clone-93207",
  storageBucket: "tinder-clone-93207.appspot.com",
  messagingSenderId: "1060398820635",
  appId: "1:1060398820635:web:08e9c5f390e0ce0047f7be",
  measurementId: "G-Q6L5GXZ4X5",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();

export default db;
