import { Avatar } from "@material-ui/core";
import React from "react";
import "./Profile.css";
import SettingsIcon from "@material-ui/icons/Settings";
import SecurityIcon from "@material-ui/icons/Security";
import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";

function Profile({ name, employment, age }) {
  const useStyles = makeStyles((theme) => ({
    root: {
      display: "flex",
      "& > *": {
        margin: theme.spacing(1),
      },
    },
    small: {
      width: theme.spacing(3),
      height: theme.spacing(3),
    },
    large: {
      width: theme.spacing(15),
      height: theme.spacing(15),
    },
  }));

  const classes = useStyles();

  return (
    <div>
      <center>
        <IconButton>
          <Avatar
            src="https://pbs.twimg.com/profile_images/1217259689072824320/HA0yuBZL_400x400.jpg"
            className={classes.large}
          />
        </IconButton>

        <div className="profile-info">
          <h3>
            {name}, {age}
          </h3>
          <p>{employment}</p>
        </div>

        <div className="profile-buttons-container">
          <IconButton>
            <SettingsIcon fontSize="default" className="settings-icon" />
          </IconButton>
          <IconButton className="security-icon">
            <SecurityIcon fontSize="default" />
          </IconButton>
        </div>
      </center>
    </div>
  );
}

export default Profile;
