import React from "react";
import PersonIcon from "@material-ui/icons/Person";
import ChatBubbleIcon from "@material-ui/icons/ChatBubble";
import "./Header.css";
import logo from "./assets/tlogo.png";
import IconButton from "@material-ui/core/IconButton";
import { Link, useHistory } from "react-router-dom";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

function Header({ backButton }) {
  const history = useHistory();
  return (
    <div className="header">
      {backButton ? (
        <IconButton onClick={() => history.replace(backButton)}>
          <ArrowBackIcon fontSize="default" className="icon" />
        </IconButton>
      ) : (
        <Link to="/profile">
          <IconButton>
            <PersonIcon fontSize="default" className="icon" />
          </IconButton>
        </Link>
      )}
      {/* {Using material ui icons } */}

      <Link to="/">
        <IconButton>
          <img src={logo} alt="tinder" className="logo" />
        </IconButton>
      </Link>
      <Link to="/chats">
        <IconButton>
          <ChatBubbleIcon fontSize="default" className="icon" />
        </IconButton>
      </Link>
    </div>
  );
}

export default Header;
